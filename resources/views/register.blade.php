<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Hari 1 - Berlatih HTML</title>
</head>
<body>
    <form action="/welcome" method="POST">
        @csrf
        <h1>Buat Account Baru!</h1>
        <h3>Sign Up Form</h3>
        First name:
        <br><br>
        <input type="text" name="firstname" value="">
        <br><br>
        Last name:
        <br><br>
        <input type="text" name="lastname" value="">
        <br><br>
        Gender:
        <br><br>
        <input type="radio" name="gender"> Male<br>
        <input type="radio" name="gender"> Female<br>
        <input type="radio" name="gender"> Other<br>
        <br>
        Nationality:
        <br><br>
        <select name="nationality">
            <option value="Indonesian">Indonesian</option>
            <option value="Singaporean">Singaporean</option>
            <option value="Malaysian">Malaysian</option>
            <option value="Australian">Australian</option>
        </select>
        <br><br>
        Language Spoken:
        <br><br>
        <input type="checkbox" name="language"> Bahasa Indonesia<br>
        <input type="checkbox" name="language"> English<br>
        <input type="checkbox" name="language"> Other<br>
        <br>
        Bio:
        <br><br>
        <textarea name="bio" id="bio" cols="30" rows="10"></textarea><br>
        <input type="submit" value="Sign Up">
    </form>
</body>
</html>