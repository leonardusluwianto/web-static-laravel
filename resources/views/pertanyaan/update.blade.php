@extends('adminlte.master')

@section('content')
    <div class="mt-3 ml-3">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">List of Questions</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
        <a class="btn btn-primary mb-2" href="/pertanyaan/create">Ask a question</a>
          <table class="table table-bordered">
            <thead>
              <tr>
                <th style="width: 10px">No</th>
                <th>Title</th>
                <th>Body</th>
                <th style="width: 40px">Action</th>
              </tr>
            </thead>
            <tbody>
              @forelse($daftar_pertanyaan as $key => $pertanyaan)
                <tr>
                  <td> {{ $key + 1 }} </td>
                  <td> {{ $pertanyaan -> judul }} </td>
                  <td> {{ $pertanyaan -> isi }} </td>
                </tr>
              @empty
                <tr>
                  <td colspan="4" align="center">No questions</td>
                </tr>
              @endforelse
            </tbody>
          </table>
        </div>
        <!-- /.card-body -->
        <!-- <div class="card-footer clearfix">
          <ul class="pagination pagination-sm m-0 float-right">
            <li class="page-item"><a class="page-link" href="#">«</a></li>
            <li class="page-item"><a class="page-link" href="#">1</a></li>
            <li class="page-item"><a class="page-link" href="#">2</a></li>
            <li class="page-item"><a class="page-link" href="#">3</a></li>
            <li class="page-item"><a class="page-link" href="#">»</a></li>
          </ul>
        </div> -->
      </div>
    </div>
@endsection