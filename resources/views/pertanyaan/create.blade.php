@extends('adminlte.master')

@section('content')
    <div class="ml-3 mt-3 mr-3">
      <div class="card card-primary">
         <div class="card-header">
           <h3 class="card-title">Ask a New Question</h3>
         </div>
         <!-- /.card-header -->
         <!-- form start -->
         <form role="form" action="/pertanyaan" method="POST">
           @csrf
           <div class="card-body">
             <div class="form-group">
               <label for="title">Title</label>
               <input type="title" class="form-control" id="title" name="title" value="{{ old('title', '') }}" placeholder="Enter question title here">
               <!-- @error('judul')
                   <div class="alert alert-danger">{{ $message }}</div>
               @enderror -->
             </div>
             <div class="form-group">
               <label for="body">Body</label>
               <input type="body" class="form-control" id="body" name="body" value="{{ old('body', '') }}" placeholder="Add a more detail description of your question...">
               <!-- @error('isi')
                   <div class="alert alert-danger">{{ $message }}</div>
               @enderror -->
               
             </div>
           </div>
           <!-- /.card-body -->

           <div class="card-footer">
             <button type="submit" class="btn btn-primary">Create</button>
           </div>
         </form>
       </div>
    </div>

@endsection