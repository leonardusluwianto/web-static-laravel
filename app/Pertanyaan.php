<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pertanyaan extends Model
{
    protected $table = "pertanyaan";

    // Metode Mass Assignmnet pada Eloquent (atribut di form yang bisa diisi)
    // protected $fillable = ["title", "body"]; 
    
    // Metode Mass Assignmnet pada Eloquent (atribut di form yang tidak bisa diisi)
    // protected $guarded = []; 
}