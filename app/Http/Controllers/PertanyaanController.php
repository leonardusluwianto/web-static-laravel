<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Pertanyaan;

class PertanyaanController extends Controller
{
    public function create() {
        return view('pertanyaan.create');
    }
    
    public function store(Request $request) {
        /*
        $validatedData = $request->validate([
            'judul' => 'required|unique:pertanyaan',
            'isi' => 'required',
        ]);
        */
        $query = DB::table('pertanyaan')->insert([
            "judul" => $request["title"],
            "isi" => $request["body"]
        ]);
        /*
        // Menyimpan data baru ke database menggunakan Eloquent ORM
        
        $pertanyaan = new Pertanyaan;
        $pertanyaan->judul = $request["title"];
        $pertanyaan->isi = $request["body"];
        $pertanyaan->save();
        
        // Menyimpan data baru ke database menggunakan metode Mass Assignment
        $pertanyaan = Pertanyaan::create([
            "judul" => $request["title"],
            "isi" => $request["body"]
        ]);
        */
        return redirect('/pertanyaan')->with('success', 'Your question has been published.');
    }

    public function index() {
        $daftar_pertanyaan = DB::table('pertanyaan')->get();
        return view('pertanyaan.index', compact('daftar_pertanyaan'));
    }

    public function show($pertanyaan_id)
    {
        $pertanyaan = DB::table('pertanyaan')->where('id', $pertanyaan_id)->first();
        return view('pertanyaan.show', compact('pertanyaan'));
    }

    public function edit($pertanyaan_id)
    {
        $pertanyaan = DB::table('pertanyaan')->where('id', $pertanyaan_id)->first();
        return view('pertanyaan.edit', compact('pertanyaan'));
    }

    public function update($pertanyaan_id, Request $request)
    {
        $query = DB::table('pertanyaan')
            ->where('id', $pertanyaan_id)
            ->update([
                'judul' => $request["title"],
                'isi' => $request["body"]
            ]);

        return redirect('/pertanyaan')->with('success', 'The question has been updated.');
    }

    public function destroy($pertanyaan_id)
    {
        $query = DB::table('pertanyaan')->where('id', $pertanyaan_id)->delete();
        return redirect('/pertanyaan')->with('danger', 'The question has been deleted.');
    }
}
